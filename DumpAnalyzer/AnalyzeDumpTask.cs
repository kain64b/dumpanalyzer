﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DumpAnalyzer
{
    class AnalyzeDumpTask: IFileSystemTask
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void Do(string file)
        {
            WaitWhileFileBusy(file);

            log.Debug("Start analyze: " + file);
            FileInfo fi = new FileInfo(file);
            Process process = new Process();
            string exepath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string bat = Path.Combine(exepath, "analyze.bat");
            DirectoryInfo dinfo = new DirectoryInfo(fi.Directory.FullName);

            var args = "\"" + file + "\" ";
            args += dinfo.Parent.FullName + "\\bin" + " ";
            args +=dinfo.Parent.FullName + "\\cpp ";
            
            var info = new ProcessStartInfo(bat, args) { WorkingDirectory = fi.Directory.FullName };
            process.StartInfo = info;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            File.WriteAllText(file + ".txt", process.StandardOutput.ReadToEnd());
            process.WaitForExit();
            log.Debug("End analyze: " + file);
        }

        private static void WaitWhileFileBusy(string file)
        {
            bool fileGood = false;
            while (!fileGood)
            {
                try
                {
                    using (var stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite))
                    {
                        fileGood = stream != null;
                    }
                }
                catch { }
                Task.Delay(100);
            }
        }
    }
}
