﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace DumpAnalyzer
{
    class DumpMonitor:IDisposable
    {
        private readonly List<string> _hotfixSourceFolders;
        private readonly List<FileSystemWatcher> _watchers = new List<FileSystemWatcher>();
        List<IFileSystemTask> _tasks;
        bool _disposed = false;
        Task _perdiodicTask;
        private const int MAX_PARALLEL_ANALYZING = 50;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DumpMonitor(string rootHotfixFolder,List<IFileSystemTask>tasks)
        {
            var dirs = Directory.GetDirectories(rootHotfixFolder);
            _hotfixSourceFolders = dirs.Where(dir => Directory.Exists(Path.Combine(dir, "src\\bin"))).ToList();
            InitFolderCreator();
            InitFileSystemWatchers();
            _tasks = tasks;
        }

        private void InitFileSystemWatchers()
        {
            StopAllWatchers();
            foreach (var dir in _hotfixSourceFolders)
            {
                FileSystemWatcher watcher = new FileSystemWatcher(Path.Combine(dir, "src\\Dumps"), "*.dmp");
                watcher.Created += new FileSystemEventHandler(DumpFileCreatedInTheFolder);
                watcher.IncludeSubdirectories = true;
                watcher.EnableRaisingEvents = true;
                _watchers.Add(watcher);
            }
        }

        private void StopAllWatchers()
        {
            foreach (var watcher in _watchers)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Created -= new FileSystemEventHandler(DumpFileCreatedInTheFolder);
                watcher.Dispose();
            }
        }

        private void InitFolderCreator()
        {
            RecreateDumpFolder();
            var timeout = new TimeSpan(1, 0, 0);
            _perdiodicTask = new Task(() => {
                while (true)
                {
                    Thread.Sleep(timeout);
                    if (RecreateDumpFolder())
                    {
                        InitFileSystemWatchers();
                    }
                }
                });
        }

        private bool RecreateDumpFolder()
        {
            bool res = false;
            foreach (var dir in _hotfixSourceFolders)
            {
                string dmpPath = Path.Combine(dir, "src\\Dumps");
                if (!Directory.Exists(dmpPath))
                {
                    res = true;
                    log.Debug("Creating Directory: " + dmpPath);
                    Directory.CreateDirectory(dmpPath);
                }
            }
            return res;
        }

        void DumpFileCreatedInTheFolder(object sender, FileSystemEventArgs e)
        {
            ThreadPool.SetMaxThreads(MAX_PARALLEL_ANALYZING, MAX_PARALLEL_ANALYZING);
            
            foreach(var task in _tasks)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback((arg) => task.Do((string)arg)), e.FullPath);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);  
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    StopAllWatchers();
                }
            }
        }

        #endregion
    }
}
