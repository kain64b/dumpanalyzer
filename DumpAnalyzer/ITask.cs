﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumpAnalyzer
{
    interface IFileSystemTask
    {
        void Do(string file);
    }
}
