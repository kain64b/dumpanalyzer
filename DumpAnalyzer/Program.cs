﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumpAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length<1||string.IsNullOrWhiteSpace(args[0])){
                Console.WriteLine("Specify hotfix root folders with delimetr \";\"");
                return;
            }
            log4net.Config.BasicConfigurator.Configure();
            var tasks = new List<IFileSystemTask>();
            tasks.Add(new AnalyzeDumpTask());
            List<DumpMonitor> monitors = new List<DumpMonitor>();
            foreach(var rootFolder in args[0].Split(new []{';'},StringSplitOptions.RemoveEmptyEntries)){
                monitors.Add(new DumpMonitor(rootFolder, tasks));
            }
            Console.ReadLine();
            foreach(var monitor in  monitors){
                using(monitor);
            }
        }
    }
}
